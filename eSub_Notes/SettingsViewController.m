//
//  SettingsViewController.m
//  eSub_Notes
//
//  Created by LAWRENCE SHANNON on 2/17/14.
//  Copyright (c) 2014 LAWRENCE SHANNON. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppDelegate.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self.navigationController setNavigationBarHidden:NO];
    //    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed: 0.0/255.0 green: 255.0/255.0 blue:255.0/255.0 alpha: 1.0];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIFont systemFontOfSize:17], NSFontAttributeName,
                                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                   nil];

    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
//    self.view.backgroundColor = [UIColor grayColor];

/*
    UIImage *image = [UIImage imageNamed: @"eSub_nav.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    self.navigationItem.titleView = imageView;
*/

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 70, 480, 18)];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    label.text = @"Settings";
    self.navigationItem.titleView = label;

    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }


}

- (void) viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];

//    AppDelegate *appD = (id) [[UIApplication sharedApplication] delegate];
    
}

- (void) viewDidDisappear:(BOOL)animated
{

    [super viewDidDisappear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    
    return YES;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 3;
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 100;
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10.0, 30.0, 25, 25)];
    [button addTarget:self action:@selector(myCustomFunction:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:[UIImage imageNamed:@"checkboxborder.png"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"checkboxmarkedborder.png"] forState:UIControlStateSelected];
    [cell.contentView addSubview:button];
        
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(40.0, 0, 59, 79)];
    [cell.contentView addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(100.0, 30.0, 300, 25.0)];
    label.font = [UIFont systemFontOfSize:17.0];
    label.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:label];
    
    button.tag = indexPath.row;

    if (indexPath.row == 0)
    {
        imageView.image = [UIImage imageNamed:@"DailyReports.png"];
        label.text = @"Daily Report (Web)";
        button.selected = [[[NSUserDefaults standardUserDefaults] objectForKey:kDailyReportWeb] boolValue];
    }
    if (indexPath.row == 1)
    {
        imageView.image = [UIImage imageNamed:@"ProjectSummary.png"];
        label.text = @"Project Summary";
        button.selected = [[[NSUserDefaults standardUserDefaults] objectForKey:kProjectSummary] boolValue];
    }
    if (indexPath.row == 2)
    {
        imageView.image = [UIImage imageNamed:@"PercentForecast.png"];
        label.text = @"Percent Complete Forecast";
        button.selected = [[[NSUserDefaults standardUserDefaults] objectForKey:kLaborActivity] boolValue];
    }
    return cell;
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

- (void)myCustomFunction:(id)sender
{
    
    UIButton *button = (id)sender;

    button.selected = !button.selected;
    
    switch (button.tag)
    {
        case 0:
            NSLog(@"Value : %@", [[NSUserDefaults standardUserDefaults] objectForKey:kDailyReportWeb]);
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", button.selected] forKey:kDailyReportWeb];
            break;
        case 1:
            NSLog(@"Value : %@", [[NSUserDefaults standardUserDefaults] objectForKey:kProjectSummary]);
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", button.selected] forKey:kProjectSummary];
            break;
        case 2:
            NSLog(@"Value : %@", [[NSUserDefaults standardUserDefaults] objectForKey:kLaborActivity]);
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", button.selected] forKey:kLaborActivity];
            break;

        default:
            break;
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

@end
