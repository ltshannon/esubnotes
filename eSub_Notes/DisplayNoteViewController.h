//
//  DisplayNoteViewController.h
//  eSub_Notes
//
//  Created by LAWRENCE SHANNON on 2/14/14.
//  Copyright (c) 2014 LAWRENCE SHANNON. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteObject.h"

@interface DisplayNoteViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView      *noteImageView;
@property (strong) NoteObject                           *note;
@property (strong, nonatomic) IBOutlet UITextView       *locationTextview;
@property (strong, nonatomic) IBOutlet UITextView       *noteTextview;
@property (strong, nonatomic) IBOutlet UITextView       *dateTextview;

@end
