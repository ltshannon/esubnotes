//
//  MaterialsTableViewController.m
//  eSUB
//
//  Created by LAWRENCE SHANNON on 1/21/15.
//  Copyright (c) 2015 LAWRENCE SHANNON. All rights reserved.
//

#import "MaterialsTableViewController.h"
#import "AppDelegate.h"
#import "DejalActivityView.h"
#import "NoteAFHTTPClient.h"
#import "MaterialsObject.h"
#import "MaterialsEntryTableViewController.h"

@interface MaterialsTableViewController ()

@end

@implementation MaterialsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIFont systemFontOfSize:17], NSFontAttributeName,
                                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                   nil];
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIButton *sButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    sButton.frame = CGRectMake(0, 0, 40, 40);
    sButton.tintColor = [UIColor blackColor];
    [sButton addTarget:self action:@selector(addMaterials) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settings = [[UIBarButtonItem alloc] initWithCustomView:sButton];
    UIBarButtonItem *editBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:settings, editBarButtonItem, nil];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(reloadEquipment:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 70, 480, 18)];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    label.text = @"Material";
    self.navigationItem.titleView = label;

}

- (void) viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    [self startGettingData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) editAction
{
    
    [self.tableView setEditing:YES];
    self.navigationItem.rightBarButtonItem = nil;
    UIBarButtonItem *editBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:editBarButtonItem, nil];
    
}

- (void) doneAction
{
    
    [self.tableView setEditing:NO];
    self.navigationItem.rightBarButtonItem = nil;
    UIButton *sButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    sButton.frame = CGRectMake(0, 0, 40, 40);
    sButton.tintColor = [UIColor blackColor];
    [sButton addTarget:self action:@selector(addMaterials) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settings = [[UIBarButtonItem alloc] initWithCustomView:sButton];
    UIBarButtonItem *editBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:settings, editBarButtonItem, nil];
    
}

- (void) addMaterials
{
    
    MaterialsEntryTableViewController *vc = [[MaterialsEntryTableViewController alloc] initWithNibName:@"MaterialsEntryTableViewController" bundle:nil];
    vc.projectMaterials = self.projectMaterials;
    vc.dailyReportId = self.dailyReportId;

    NSMutableArray *perList = [[NSMutableArray alloc] init];
    [perList addObject:@"Barrel"];
    [perList addObject:@"Box"];
    [perList addObject:@"Bunch"];
    [perList addObject:@"Bundle of sticks"];
    [perList addObject:@"Color"];
    [perList addObject:@"Foot"];
    [perList addObject:@"Govenor"];
    [perList addObject:@"Pound"];
    
    vc.newEntry = YES;
    vc.perList = perList;

    
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (BOOL)prefersStatusBarHidden
{
    
    return YES;
    
}

- (void) reloadEquipment:(UIRefreshControl *)refreshControl
{
    
    [self startGettingData];
    [refreshControl endRefreshing];
    
}

- (void) startGettingData
{
    
    AppDelegate *appD = (id) [[UIApplication sharedApplication] delegate];
    
    if ([appD.reachability currentReachabilityStatus] == NotReachable)
    {
        self.projectMaterials = [appD.eSubsDB getProjectMaterialObjects:self.projectId];
//        self.listOfEquipment = [appD.eSubsDB getEquipmentObjects:self.projectId];
        [self.tableView reloadData];
    }
    else
    {
        [self getListOfProjectMaterials];
//        [self getListOfEquipment];
    }
    
}

- (void) getListOfProjectMaterials
{
    
    AppDelegate *appD = (id) [[UIApplication sharedApplication] delegate];
    
    [DejalBezelActivityView activityViewForView:self.view];
    
    NoteAFHTTPClient *client = [NoteAFHTTPClient sharedClient:[NSURL URLWithString:appD.eSUBServerURL]];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:(int)self.projectId]  forKey:@"projectID"];
    
    NSURLRequest *request = [client requestWithMethod:@"GET" path:@"Material" parameters:params];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id responseObject)
        {
            [DejalBezelActivityView removeViewAnimated:YES];
            [appD.eSubsDB deleteProjectEquipmentObjects:self.projectId];
            NSArray *data = [responseObject objectForKey:@"data"];
            if (data.count)
            {
                self.projectMaterials = [[NSMutableArray alloc] init];
                for (NSDictionary *mdata in data)
                {
                    MaterialsObject *material = [[MaterialsObject alloc] init];
                    if ([mdata objectForKey:@"materialId"] != [NSNull null])
                    {
                        material.Id = [[mdata objectForKey:@"materialId"] integerValue];
                    }
                    if ([mdata objectForKey:@"materialName"] != [NSNull null])
                    {
                        material.name = [mdata objectForKey:@"materialName"];
                    }
                    if ([mdata objectForKey:@"materialQuantityValue"] != [NSNull null])
                    {
                        material.quantityValue = [mdata objectForKey:@"materialQuantityValue"];
                    }
                    if ([mdata objectForKey:@"materialPerValue"] != [NSNull null])
                    {
                        material.perValue = [mdata objectForKey:@"materialPerValue"];
                    }
                    if ([mdata objectForKey:@"materialNotesValue"] != [NSNull null])
                    {
                        material.notesValue = [mdata objectForKey:@"materialNotesValue"];
                    }
                    [self.projectMaterials addObject:material];
                    [appD.eSubsDB insertProjectMaterialObject:material forProjectId:self.projectId];
                }
            }

        }
        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
        {
            [DejalBezelActivityView removeViewAnimated:YES];
            NSLog(@"Network error in materials download %@", [error localizedDescription]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }];
    
    [operation start];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.projectMaterials.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    
    MaterialsObject *material = [self.projectMaterials objectAtIndex:indexPath.row];
    
    cell.textLabel.text = material.name;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return YES;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Delete the row from the data source
        EquipmentObject *equipment = [self.projectMaterials objectAtIndex:indexPath.row];
        [self.projectMaterials removeObject:equipment];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    MaterialsObject *material = [self.projectMaterials objectAtIndex:indexPath.row];
    MaterialsEntryTableViewController *vc = [[MaterialsEntryTableViewController alloc] initWithNibName:@"MaterialsEntryTableViewController" bundle:nil];
    NSMutableArray *perList = [[NSMutableArray alloc] init];
    [perList addObject:@"Barrel"];
    [perList addObject:@"Box"];
    [perList addObject:@"Bunch"];
    [perList addObject:@"Bundle of sticks"];
    [perList addObject:@"Color"];
    [perList addObject:@"Foot"];
    [perList addObject:@"Govenor"];
    [perList addObject:@"Pound"];
    vc.perList = perList;
    
    vc.projectMaterials = self.projectMaterials;
    vc.dailyReportId = self.dailyReportId;

    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
