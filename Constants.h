//
//  Constants.h
//  eSub_Notes
//
//  Created by LAWRENCE SHANNON on 2/18/14.
//  Copyright (c) 2014 LAWRENCE SHANNON. All rights reserved.
//

#define kRememberedSubscriberDataKey @"login_subscriber"
#define kRememberedEmailDataKey @"login_email"
#define kRememberedPasswordDataKey @"login_password"
#define kRememberedRememberFlagDataKey @"login_remember"
#define kDailyReportWeb @"dailyReportWeb"
#define kDailyReport @"dailyReport"
#define kProjectSummary @"projectSummary"
#define kLaborActivity @"laborActivty"

#define kNewNoteCount @"new_note_count"
#define kNewDRCount @"new_dr_count"
#define kNewCrewCount @"new_crew_count"
#define kNewEquipmentCount @"new_equipment_count"

#define keSUBQAServerURL @"http://api.qa.esubonline.com/v2/"
#define keSUBBETAServerURL @"https://api.beta.esubonline.com/v2/"
#define keSUBServerURL @"https://api.esubonline.com/v2/"

#ifndef eSub_Notes_Constants_h
#define eSub_Notes_Constants_h



#endif
